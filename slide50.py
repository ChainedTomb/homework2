#!/usr/bin/env python

__version__ = '1.0'
__author__ = 'Piotr Szulc'
__email__ = 'piotrszulc1011@gmail.com'
__copyright__ = "Copyright (c) 2020 Piotr Szulc"

"""Zadanie ze slajdu 50"""

class Tower:
    def __init__(self):
        self.origin = str("Yes")


    def buy_unit(self):
        print("I'm a Tower's unit")


class Tavern(Tower):
    def __init__(self):
        super(Tavern, self).__init__()
        print("Am I Tower's hero? ", self.origin)


class CloudTemple(Tower):
    def __init__(self):
        Tower.buy_unit("Tower unit")
        print("Tier 7 Giant")


class UpgrCloudTemple(Tower):
    def buy_unit(self):
        super(UpgrCloudTemple, self).buy_unit()
        print("Tier 7 Titan - Upgraded Giant")

Solmyr = Tavern()
print(Solmyr, "\n")
giant = CloudTemple()
print(giant, "\n")
UpgrCloudTemple.buy_unit(UpgrCloudTemple())