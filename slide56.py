#!/usr/bin/env python

__version__ = '1.0'
__author__ = 'Piotr Szulc'
__email__ = 'piotrszulc1011@gmail.com'
__copyright__ = "Copyright (c) 2020 Piotr Szulc"

"""Zadanie ze slajdu 56"""

name = input("Give me your name: ")
surname = input("Give me your surname: ")
age = input("Give me your age: ")

print (name, surname, age)

def convert():
    number: str = input("Give me number to convert: ")
    number: int = int(number)
    print(number, format(type(number)))

convert()

class Tulip:
    def __init__(self, flower_color, stem_color):
        print("Tulip is ", flower_color, " and his stem is ", stem_color)


flower_color = input("What color does Tulip has?: ")
stem_color = input("What color does Tulip's stem has?: ")
make = Tulip(flower_color, stem_color)
