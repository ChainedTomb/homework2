#!/usr/bin/env python

__version__ = '1.0'
__author__ = 'Piotr Szulc'
__email__ = 'piotrszulc1011@gmail.com'
__copyright__ = "Copyright (c) 2020 Piotr Szulc"

"""Zadanie ze slajdu 71"""

"""Podpunkt 1"""


name = input("Enter your name: ")

if name in "Piotr":
    print("Wow, you have the same name")
else:
    print("Hello", name)

"""Podpunkt 2"""


class Rectangle:
    def __init__(self, side_a: int, side_b: int):
        self.side_a: int = side_a
        self.side_b: int = side_b

    def __eq__(self, other):
        return self.side_a == other.side_a and self.side_b == other.side_b

    def __gt__(self, other):
        return self.side_a > other.side_a and self.side_b > other.side_b

    def __lt__(self, other):
        return self.side_a < other.side_a and self.side_b < other.side_b

    def __ge__(self, other):
        return self.side_a >= other.side_a and self.side_b >= other.side_b

    def __le__(self, other):
        return self.side_a <= other.side_a and self.side_b <= other.side_b

    def __ne__(self, other):
        return self.side_a != other.side_a and self.side_b != other.side_b


figure = Rectangle(3, 5)
figure_2 = Rectangle (5, 7)

print(format(figure >= figure_2))


"""Podpunkt 3"""


class Cube:
    def __init__(self, side_a: int):
        self.capacity: int = side_a * side_a * side_a

    def __eq__(self, other):
        return self.capacity == other.capacity

    def __gt__(self, other):
        return self.capacity > other.capacity

    def __lt__(self, other):
        return self.capacity < other.capacity

    def __ge__(self, other):
         return self.capacity >= other.capacity

    def __le__(self, other):
        return self.capacity <= other.capacity

    def __ne__(self, other):
        return self.capacity != other.capacity


chunk = Cube(3)
chunk_2 = Cube(4)

print(format(chunk < chunk_2))


"""Podpunkt 4"""


class Class:
    def __init__(self, lvl):
        self.lvl = lvl

    def __eq__(self, other):
        return self.lvl == other.lvl

    def __gt__(self, other):
        return self.lvl > other.lvl

    def __lt__(self, other):
        return self.lvl < other.lvl

    def __ge__(self, other):
        return self.lvl >= other.lvl

    def __le__(self, other):
        return self.lvl <= other.lvl

    def __ne__(self, other):
        return self.lvl != other.lvl


class Warrior(Class):
    def __init__(self, lvl):
        super(Warrior, self).__init__(lvl)
        self.lvl = lvl

    def __eq__(self, other):
        return self.lvl == other.lvl

    def __gt__(self, other):
        return self.lvl > other.lvl

    def __lt__(self, other):
        return self.lvl < other.lvl

    def __ge__(self, other):
        return self.lvl >= other.lvl

    def __le__(self, other):
        return self.lvl <= other.lvl

    def __ne__(self, other):
        return self.lvl != other.lvl



class Mage(Class):
    def __init__(self, lvl):
        super(Mage, self).__init__(lvl)
        self.lvl = lvl

    def __eq__(self, other):
        return self.lvl == other.lvl

    def __gt__(self, other):
        return self.lvl > other.lvl

    def __lt__(self, other):
        return self.lvl < other.lvl

    def __ge__(self, other):
        return self.lvl >= other.lvl

    def __le__(self, other):
        return self.lvl <= other.lvl

    def __ne__(self, other):
        return self.lvl != other.lvl



class Berserker(Warrior):
    def __init__(self, lvl):
        super(Berserker, self).__init__(lvl)
        self.lvl = lvl

    def __eq__(self, other):
        return self.lvl == other.lvl

    def __gt__(self, other):
        return self.lvl > other.lvl

    def __lt__(self, other):
        return self.lvl < other.lvl

    def __ge__(self, other):
        return self.lvl >= other.lvl

    def __le__(self, other):
        return self.lvl <= other.lvl

    def __ne__(self, other):
        return self.lvl != other.lvl


class Necromancer(Mage):
    def __init__(self, lvl):
        super(Necromancer, self).__init__(lvl)
        self.lvl = lvl

    def __eq__(self, other):
        return self.lvl == other.lvl

    def __gt__(self, other):
        return self.lvl > other.lvl

    def __lt__(self, other):
        return self.lvl < other.lvl

    def __ge__(self, other):
        return self.lvl >= other.lvl

    def __le__(self, other):
        return self.lvl <= other.lvl

    def __ne__(self, other):
        return self.lvl != other.lvl


champ1 = Warrior(50)
champ2 = Necromancer(55)

print(format(Warrior != Necromancer))

