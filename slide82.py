#!/usr/bin/env python

__version__ = '1.0'
__author__ = 'Piotr Szulc'
__email__ = 'piotrszulc1011@gmail.com'
__copyright__ = "Copyright (c) 2020 Piotr Szulc"

"""Zadanie ze slajdu 82"""


big_list = [
    [1],
    [2],
    [3]
]

print(big_list)

empty_list = []
for num in range(100, 116):
    empty_list.append(num)

print(empty_list)

list_float = [3.14, 4.21, 5.56, 9.34, 12.85]
for num in list_float:
    print(num, end=" ")

