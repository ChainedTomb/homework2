#!/usr/bin/env python

__version__ = '1.0'
__author__ = 'Piotr Szulc'
__email__ = 'piotrszulc1011@gmail.com'
__copyright__ = "Copyright (c) 2020 Piotr Szulc"

"""Zadanie ze slajdu 41"""

def print_my_name(name: str) -> None:
    print ("Your name is", name)

print_my_name("Piotr")


def add_numbers(a: int, b: int, c: int) -> int:
    print(a+b+c)

add_numbers(1, 2, 3)


def greetings() -> None:
    print("Hello World")

greetings()


def substract_numbers(a: int, b: int) -> int:
    return a-b

print(substract_numbers(4, 2))


class Tool: pass

def tool_creator() -> Tool:
    return Tool()

vacuum = tool_creator()
print(isinstance(vacuum, Tool))
print(vacuum)